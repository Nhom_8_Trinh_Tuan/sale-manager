package com.example.CircuitBreakerMicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CircuitBreakerMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CircuitBreakerMicroserviceApplication.class, args);
	}

}
