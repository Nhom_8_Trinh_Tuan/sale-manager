package com.example.CircuitBreakerMicroservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

public class CatalogServiceCaller {
	@Autowired
    private RestTemplate restTemplate;

    @CircuitBreaker(name = "catalogService", fallbackMethod = "fallbackMethod")
    public String callCatalogService() {
        return restTemplate.getForObject("http://localhost:8803/catalog", String.class);
    }

    public String fallbackMethod(Exception ex) {
        return "Fallback response: Service is unavailable";
    }
}
