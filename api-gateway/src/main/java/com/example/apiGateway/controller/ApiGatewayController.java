package com.example.apiGateway.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiGatewayController {
	@GetMapping("/api/gateway")
	public String getApiGateway() {
		return "API-GATEWAY-SERVICE";
	}
}
