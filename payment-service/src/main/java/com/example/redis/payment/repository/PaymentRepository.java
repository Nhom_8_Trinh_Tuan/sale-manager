package com.example.redis.payment.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.example.redis.payment.entity.Payment;

@Repository
public interface PaymentRepository extends MongoRepository<Payment, Integer> {

}
