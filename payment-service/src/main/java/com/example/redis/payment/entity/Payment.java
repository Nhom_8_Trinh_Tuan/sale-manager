package com.example.redis.payment.entity;

import java.io.Serializable;
import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

@Document(collection = "Payment")
public class Payment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3243815877427206478L;
	@Id
	private int id;
	private LocalDate dateOfPayment;
	private double amountOfMoney;
	private String paymentMethod;

	public Payment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Payment(int id, LocalDate dateOfPayment, double amountOfMoney, String paymentMethod) {
		super();
		this.id = id;
		this.dateOfPayment = dateOfPayment;
		this.amountOfMoney = amountOfMoney;
		this.paymentMethod = paymentMethod;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getDateOfPayment() {
		return dateOfPayment;
	}

	public void setDateOfPayment(LocalDate dateOfPayment) {
		this.dateOfPayment = dateOfPayment;
	}

	public double getAmountOfMoney() {
		return amountOfMoney;
	}

	public void setAmountOfMoney(double amountOfMoney) {
		this.amountOfMoney = amountOfMoney;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

}
