package com.example.redis.payment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.web.bind.annotation.*;
import com.example.redis.payment.entity.Payment;
import com.example.redis.payment.repository.PaymentRepository;
import java.util.List;

@RestController
@RequestMapping("/payment")
public class PaymentServiceController {

	@Autowired
	private PaymentRepository paymentRepository;

	@PostMapping
	@CachePut(value = "payment", key = "#id")
	public Payment save(@RequestBody Payment payment) {
		return paymentRepository.save(payment);
	}

	@PutMapping("/{id}")
	@CachePut(value = "payment", key = "#id")
	public Payment updatePayment(@PathVariable int id, @RequestBody Payment payment) {
		payment.setId(id);
		return paymentRepository.save(payment);
	}

	@GetMapping
	@CachePut(value = "payment", key = "#payment.id")
	public List<Payment> getAllPayments() {
		return paymentRepository.findAll();
	}

	@GetMapping("/{id}")
	@CachePut(value = "payment", key = "#id")
	public Payment findPayment(@PathVariable int id) {
		return paymentRepository.findById(id).orElse(null);
	}

	@DeleteMapping("/{id}")
	@CachePut(value = "payment", key = "#id")
	public void removePayment(@PathVariable int id) {
		paymentRepository.deleteById(id);
	}
}
