package com.example.redis.inventory.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.example.redis.inventory.entity.Inventory;

@Repository
public interface InventoryRepository extends MongoRepository<Inventory, Integer> {
	
}
