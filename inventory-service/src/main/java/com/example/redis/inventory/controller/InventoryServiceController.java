package com.example.redis.inventory.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.web.bind.annotation.*;
import com.example.redis.inventory.entity.Inventory;
import com.example.redis.inventory.repository.InventoryRepository;
import java.util.List;

@RestController
@RequestMapping("/inventory")
public class InventoryServiceController {

	@Autowired
	private InventoryRepository inventoryRepository;

	@PostMapping
	@CachePut(value = "inventory", key = "#id")
	public Inventory save(@RequestBody Inventory inventory) {
		return inventoryRepository.save(inventory);
	}

	@PutMapping("/{id}")
	@CachePut(value = "inventory", key = "#id")
	public Inventory updateInventory(@PathVariable int id, @RequestBody Inventory inventory) {
		inventory.setId(id);
		return inventoryRepository.save(inventory);
	}

	@GetMapping
	@CachePut(value = "inventory", key = "#inventory.id")
	public List<Inventory> getAllInventories() {
		return inventoryRepository.findAll();
	}

	@GetMapping("/{id}")
	@CachePut(value = "inventory", key = "#id")
	public Inventory findInventory(@PathVariable int id) {
		return inventoryRepository.findById(id).orElse(null);
	}

	@DeleteMapping("/{id}")
	@CachePut(value = "inventory", key = "#id")
	public void removeInventory(@PathVariable int id) {
		inventoryRepository.deleteById(id);
	}
}
