package com.example.redis.customer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.web.bind.annotation.*;
import com.example.redis.customer.entity.Customer;
import com.example.redis.customer.repository.CustomerRepository;
import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerServiceController {

	@Autowired
	private CustomerRepository customerRepository;

	@PostMapping
	@CachePut(value = "customer", key = "#id")
	public Customer save(@RequestBody Customer customer) {
		return customerRepository.save(customer);
	}

	@PutMapping("/{id}")
	@CachePut(value = "customer", key = "#id")
	public Customer updateCustomer(@PathVariable int id, @RequestBody Customer customer) {
		customer.setId(id);
		return customerRepository.save(customer);
	}

	@GetMapping
	@CachePut(value = "customer", key = "#customer.id")
	public List<Customer> getAllCustomers() {
		return customerRepository.findAll();
	}

	@GetMapping("/{id}")
	@CachePut(value = "customer", key = "#id")
	public Customer findCustomer(@PathVariable int id) {
		return customerRepository.findById(id).orElse(null);
	}

	@DeleteMapping("/{id}")
	@CachePut(value = "customer", key = "#id")
	public void removeCustomer(@PathVariable int id) {
		customerRepository.deleteById(id);
	}
}
