package com.example.redis.customer.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.example.redis.customer.entity.Customer;

@Repository
public interface CustomerRepository extends MongoRepository<Customer, Integer> {
	
}

