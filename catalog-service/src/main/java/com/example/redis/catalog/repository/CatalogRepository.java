package com.example.redis.catalog.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.example.redis.catalog.entity.Catalog;

@Repository
public interface CatalogRepository extends MongoRepository<Catalog, Integer> {
	
}


