package com.example.redis.catalog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.web.bind.annotation.*;
import com.example.redis.catalog.entity.Catalog;
import com.example.redis.catalog.repository.CatalogRepository;
import java.util.List;

@RestController
@RequestMapping("/catalog")
public class CatalogServiceController {

	@Autowired
    private CatalogRepository catalogRepository;

    @PostMapping
    @CachePut(value = "catalog", key = "#id")
    public Catalog save(@RequestBody Catalog catalog) {
        return catalogRepository.save(catalog);
    }

    @PutMapping("/{id}")
    @CachePut(value = "catalog", key = "#id")
    public Catalog updateCatalog(@PathVariable int id, @RequestBody Catalog catalog) {
        catalog.setId(id); 
        return catalogRepository.save(catalog);
    }

    @GetMapping
    @CachePut(value = "catalog", key = "#catalog.id")
    public List<Catalog> getAllCatalogs() {
        return catalogRepository.findAll();
    }

    @GetMapping("/{id}")
    @CachePut(value = "catalog", key = "#id")
    public Catalog findCatalog(@PathVariable int id) {
        return catalogRepository.findById(id).orElse(null);
    }

    @DeleteMapping("/{id}")
    @CachePut(value = "catalog", key = "#id")
    public void removeCatalog(@PathVariable int id) {
        catalogRepository.deleteById(id);
    }
}
