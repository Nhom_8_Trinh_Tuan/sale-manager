package com.example.redis.order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.web.bind.annotation.*;
import com.example.redis.order.entity.Order;
import com.example.redis.order.repository.OrderRepository;
import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderServiceController {

	@Autowired
	private OrderRepository orderRepository;

	@PostMapping
	@CachePut(value = "order", key = "#id")
	public Order save(@RequestBody Order order) {
		return orderRepository.save(order);
	}

	@PutMapping("/{id}")
	@CachePut(value = "order", key = "#id")
	public Order updateOrder(@PathVariable int id, @RequestBody Order order) {
		order.setId(id);
		return orderRepository.save(order);
	}

	@GetMapping
	@CachePut(value = "order", key = "#order.id")
	public List<Order> getAllOrders() {
		return orderRepository.findAll();
	}

	@GetMapping("/{id}")
	@CachePut(value = "order", key = "#id")
	public Order findOrder(@PathVariable int id) {
		return orderRepository.findById(id).orElse(null);
	}

	@DeleteMapping("/{id}")
	@CachePut(value = "order", key = "#id")
	public void removeOrder(@PathVariable int id) {
		orderRepository.deleteById(id);
	}
}
