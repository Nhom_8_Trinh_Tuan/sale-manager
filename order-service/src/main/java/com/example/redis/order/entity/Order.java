package com.example.redis.order.entity;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

@Document(collection = "Order")
public class Order implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1634293698526166347L;
	@Id
	private int id;
	private String name;
	private int quantity;
	private String description;
	private double price;
	private String rate;
	private String status;
	private String orderConfirm;

	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Order(int id, String name, int quantity, String description, double price, String rate, String status,
			String orderConfirm) {
		super();
		this.id = id;
		this.name = name;
		this.quantity = quantity;
		this.description = description;
		this.price = price;
		this.rate = rate;
		this.status = status;
		this.orderConfirm = orderConfirm;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrderConfirm() {
		return orderConfirm;
	}

	public void setOrderConfirm(String orderConfirm) {
		this.orderConfirm = orderConfirm;
	}

}
